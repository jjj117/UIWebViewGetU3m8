//
//  ViewController.m
//  TestWebVIew
//
//  Created by shaowei on 13-10-18.
//  Copyright (c) 2013年 AISpeech-SW. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc {
    [_webView release];
    [super dealloc];
}

- (void)viewDidUnload {
    [self setWebView:nil];
    [super viewDidUnload];
}

#pragma mark - Function - Public

- (NSString *)loadWebViewWithFileName:(NSString *)fileName{
    return [[NSBundle mainBundle] pathForResource:fileName ofType:nil];
    
    
    
}

#pragma mark - action

- (IBAction)actionWebRes:(id)sender {
    NSString *filePath = [[NSBundle mainBundle] pathForResource:@"download_res.html"
                                                         ofType:nil];
    NSURL *url = [NSURL fileURLWithPath:filePath];
    NSLog(@"SW -fileURLWithPath: %@",url);
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    
    [_webView loadRequest:request];
}

- (IBAction)actionWeb_1:(id)sender {
    NSString *filePath = [[NSBundle mainBundle] pathForResource:@"download_res.html"
                                                         ofType:nil];
    NSString *fileContent = [NSString stringWithContentsOfFile:filePath
                                                      encoding:NSUTF8StringEncoding error:nil];
    NSLog(@"SW - URLWithString:%@",[NSURL URLWithString:filePath]);
    [_webView loadHTMLString:fileContent baseURL:[NSURL URLWithString:filePath]];
}

- (IBAction)actionBack:(id)sender {
    if ([_webView canGoBack]) {
        [_webView goBack];
    }
}

- (IBAction)actionForward:(id)sender {
    if ([_webView canGoForward]) {
        [_webView goForward];
    }
}

#pragma mark Function - Private




@end
