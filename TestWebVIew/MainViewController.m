//
//  MainViewController.m
//  TestWebVIew
//
//  Created by shaowei on 13-10-23.
//  Copyright (c) 2013年 AISpeech-SW. All rights reserved.
//

#import "MainViewController.h"
#import "ViewController.h"
#import "OnlineViewController.h"
#import "VideoViewController.h"

@interface MainViewController ()

@end

@implementation MainViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc {
    [_tableView release];
    [super dealloc];
}
- (void)viewDidUnload {
    [self setTableView:nil];
    [super viewDidUnload];
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 3;
}

// Row display. Implementers should *always* try to reuse cells by setting each cell's reuseIdentifier and querying for available reusable cells with dequeueReusableCellWithIdentifier:
// Cell gets various attributes set automatically based on table (separators) and data source (accessory views, editing controls)

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"ss"] autorelease];
    
    NSString *title = nil;
    
    if (indexPath.row == 0) {
        title = [NSString stringWithFormat:@"LocalWebView"];
    }else if (indexPath.row == 1){
        title = [NSString stringWithFormat:@"NetWebView"];
    }else if (indexPath.row == 2){
        title = [NSString stringWithFormat:@"Video"];
    }
    
    cell.textLabel.text = title;
    
    return cell;
}


#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    UIViewController *tmpVC = nil;
    if (indexPath.row == 0) {
        tmpVC = [[[ViewController alloc] initWithNibName:nil bundle:nil] autorelease];
    }else if (indexPath.row == 1){
        tmpVC = [[[OnlineViewController alloc] initWithNibName:nil bundle:nil] autorelease];
    }
    else if (indexPath.row == 2){
        tmpVC = [[[VideoViewController alloc] initWithNibName:nil bundle:nil] autorelease];
    }
    
    [self.navigationController pushViewController:tmpVC animated:YES];
}



@end
