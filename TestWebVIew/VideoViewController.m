//
//  VideoViewController.m
//  TestWebVIew
//
//  Created by shaowei on 13-10-23.
//  Copyright (c) 2013年 AISpeech-SW. All rights reserved.
//

#import "VideoViewController.h"
#import <MediaPlayer/MPMoviePlayerController.h>

@interface VideoViewController ()

@property (nonatomic,retain) MPMoviePlayerController *player;

@end

@implementation VideoViewController


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc {
    [_player stop];
    [_player release];
    [_textField release];
    [super dealloc];
}
- (void)viewDidUnload {
    [self setTextField:nil];
    [super viewDidUnload];
}
- (IBAction)actionGO:(id)sender {
    [self playVideo:self.textField.text];
}

#pragma mark - Function - Public

- (void)playVideo:(NSString *)m3u8String{
    [self.textField resignFirstResponder];
    [self.textField setText:m3u8String];
    
    if (!_player) {
        self.player = [[[MPMoviePlayerController alloc] initWithContentURL:nil] autorelease];
        
    }
    
    [_player setContentURL:[NSURL URLWithString:m3u8String]];
    
    [_player prepareToPlay];
    [_player.view setFrame: CGRectMake(10, 110, 300, 300)];  // player's frame must match parent's
    [self.view addSubview: _player.view];
    // ...
    [_player play];
}


@end
