//
//  AppDelegate.h
//  TestWebVIew
//
//  Created by shaowei on 13-10-18.
//  Copyright (c) 2013年 AISpeech-SW. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ViewController;

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) ViewController *viewController;

@end
