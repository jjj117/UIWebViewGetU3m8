//
//  VideoViewController.h
//  TestWebVIew
//
//  Created by shaowei on 13-10-23.
//  Copyright (c) 2013年 AISpeech-SW. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VideoViewController : UIViewController

@property (retain, nonatomic) IBOutlet UITextField *textField;

- (IBAction)actionGO:(id)sender;
- (void)playVideo:(NSString *)m3u8String;

@end
