//
//  main.m
//  TestWebVIew
//
//  Created by shaowei on 13-10-18.
//  Copyright (c) 2013年 AISpeech-SW. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
